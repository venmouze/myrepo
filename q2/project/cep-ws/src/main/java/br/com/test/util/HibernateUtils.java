package br.com.test.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class HibernateUtils {
	
	public static EntityManager getEntityManager(String entity) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory(entity);
		EntityManager entityManager = factory.createEntityManager();
		
		return entityManager;
	}
	
	public static void closeEntityManager(EntityManager entityManager) {

		if (entityManager != null) {
			entityManager.close();
		}
	}	
}
