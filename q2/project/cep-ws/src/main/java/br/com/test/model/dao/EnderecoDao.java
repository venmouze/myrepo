package br.com.test.model.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;

import br.com.test.model.Endereco;

public class EnderecoDao {

	private static EntityManager entityManager;
	
	public static Endereco load(Integer cepId) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("cep-ws");
		EntityManager entityManager = factory.createEntityManager();
		Endereco endereco = null;
		//int count;
		
		try {
			endereco = entityManager.find(Endereco.class, cepId);
			//String newCep = String.valueOf(cepId);
			//count = newCep.length();
			
			/*
			while (endereco.getEndereco() == null || endereco.getEndereco().isEmpty() && count != 0) {
				newCep = newCep.substring(0, newCep.length() - 1);
				System.out.println("newcep: " + newCep);
				endereco = entityManager.find(Enderecos.class, Integer.valueOf(newCep.concat("0")));
			}
			*/
			
			return endereco;
		} catch (NoResultException e) {
			return null;
		} finally {
			close();
		}
	}
	
	private static void close() {
		if (entityManager != null) {
			entityManager.close();
		}
	}	
}
