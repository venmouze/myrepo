package br.com.test.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.test.model.Endereco;
import br.com.test.model.dao.EnderecoDao;


@Path("address")
public class CepService {
		
	@POST
	@Path("getAddressByCep")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Endereco getAddressByCep(generated.Enderecos enderecoEntity) {
		Endereco endereco = EnderecoDao.load(enderecoEntity.getCepId());
		
		return endereco;
	}
}
