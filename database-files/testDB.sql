--create database
CREATE DATABASE testDB;

--use database
USE testDB;

--create table usuario
CREATE TABLE usuario (
	usuario_id INTEGER NOT NULL AUTO_INCREMENT, 
	apelido VARCHAR(25), 
	nome VARCHAR(40),
	CONSTRAINT usuario_usuario_id_pk PRIMARY KEY (usuario_id));

--create table endereco
CREATE TABLE endereco (
	cep_id INTEGER(8) NOT NULL, 
	endereco VARCHAR(100), 
	cidade VARCHAR(50), 
	estado VARCHAR(2),
	bairro VARCHAR(50),
	CONSTRAINT endereco_cep_id_pk PRIMARY KEY(cep_id));

--create table endereco_usuario
CREATE TABLE endereco_usuario (
	endereco_id INTEGER NOT NULL AUTO_INCREMENT,
	nome_referencia VARCHAR(25),
	numero INTEGER(5),
	complemento VARCHAR(100),
	usuario_id INTEGER NOT NULL,
	cep_id INTEGER NOT NULL,
	CONSTRAINT endereco_usuario_endereco_id_pk PRIMARY KEY(endereco_id),
	CONSTRAINT endereco_usuario_usuario_id_fk FOREIGN KEY endereco_usuario(usuario_id) REFERENCES usuario(usuario_id),
	CONSTRAINT endereco_usuario_cep_id_fk FOREIGN KEY endereco_usuario(cep_id) REFERENCES endereco(cep_id));
	
--fill table usuario
INSERT INTO usuario (apelido, nome) VALUES ('maria','Maria');
INSERT INTO usuario (apelido, nome) VALUES ('joao','João');
INSERT INTO usuario (apelido, nome) VALUES ('ana','Ana');
INSERT INTO usuario (apelido, nome) VALUES ('aline','Aline');
INSERT INTO usuario (apelido, nome) VALUES ('willian','Willian');

--fill table endereco
INSERT INTO endereco (cep_id, endereco, cidade, estado, bairro) VALUES ('03650000', 'Rua das Letras', 'São Paulo', 'SP', 'Vila Fan');
INSERT INTO endereco (cep_id, endereco, cidade, estado, bairro) VALUES ('03650010', 'Rua das Ruas', 'São Paulo', 'SP', 'Vila Fan');
INSERT INTO endereco (cep_id, endereco, cidade, estado, bairro) VALUES ('03650020', 'Rua Grande', 'São Paulo', 'RJ', 'Vila Joá');
INSERT INTO endereco (cep_id, endereco, cidade, estado, bairro) VALUES ('03650030', 'Rua Pequena', 'São Paulo', 'ES', 'Vila Non');
INSERT INTO endereco (cep_id, endereco, cidade, estado, bairro) VALUES ('03650040', 'Rua Velha', 'São Paulo', 'SP', 'Vila Fan');
INSERT INTO endereco (cep_id, endereco, cidade, estado, bairro) VALUES ('03650050', 'Rua Nova', 'São Paulo', 'SP', 'Vila Fan');
INSERT INTO endereco (cep_id, endereco, cidade, estado, bairro) VALUES ('03650060', 'Rua Municipal', 'São Paulo', 'SP', 'Vila Global');
INSERT INTO endereco (cep_id, endereco, cidade, estado, bairro) VALUES ('03650070', 'Rua Federal', 'São Paulo', 'SP', 'Vila Global');
INSERT INTO endereco (cep_id, endereco, cidade, estado, bairro) VALUES ('03650080', 'Rua dos Comércios', 'São Paulo', 'MG', 'Vila Grife');
INSERT INTO endereco (cep_id, endereco, cidade, estado, bairro) VALUES ('03650090', 'Rua 29 de Fevereiro', 'São Paulo', 'SP', 'Vila Longe');
INSERT INTO endereco (cep_id, endereco, cidade, estado, bairro) VALUES ('036500100', 'Rua Loga', 'São Paulo', 'SP', 'Vila Too');
INSERT INTO endereco (cep_id, endereco, cidade, estado, bairro) VALUES ('036500000', 'Rua Leguas', 'São Paulo', 'SP', 'Oriente');
INSERT INTO endereco (cep_id, endereco, cidade, estado, bairro) VALUES ('036502000', '', '', '', '');
INSERT INTO endereco (cep_id, endereco, cidade, estado, bairro) VALUES ('036502100', '', '', '', '');
INSERT INTO endereco (cep_id, endereco, cidade, estado, bairro) VALUES ('036502110', '', '', '', '');
INSERT INTO endereco (cep_id, endereco, cidade, estado, bairro) VALUES ('036502117', '', '', '', '');
INSERT INTO endereco (cep_id, endereco, cidade, estado, bairro) VALUES ('036602000', '', '', '', '');
INSERT INTO endereco (cep_id, endereco, cidade, estado, bairro) VALUES ('036602100', '', '', '', '');
INSERT INTO endereco (cep_id, endereco, cidade, estado, bairro) VALUES ('036602160', '', '', '', '');
INSERT INTO endereco (cep_id, endereco, cidade, estado, bairro) VALUES ('036602162', '', '', '', '');