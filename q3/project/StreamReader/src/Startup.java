import helper.Scanner;

/* Classe Startup
 * 
 * @author Willian
 */	
public class Startup {

	/*
	 * M�todo main, executa o m�todo firstChar
	 * Alguns STOUT est�o comentados, colocados para verificar tempo de processamento.
	 * 
	 * @return char - Retorna o primeiro caracter que n�o se repete na stream.
	 */	
	public static void main(String[] args) {
		//System.out.println("Start: " + System.nanoTime());
		char firstChar = firstChar();
		System.out.println(firstChar);
		//System.out.println("End: " + System.nanoTime());
	}
	
	/*
	 * M�todo respons�vel por utilizar o hasNext e getNext afim de receber o primeiro
	 * caracter que n�o se repete na stream.
	 * Alguns STOUT est�o comentados, colocados para verificar tempo de processamento.
	 * 
	 * @return char - Retorna o primeiro caracter que n�o se repete na stream.
	 */
	public static char firstChar() {

		String input = "aAbBABacbabbbcacvdabbacxazabazABcvdbxa";
		char returnChar = 0;
		char firstChar;
		
		Scanner scanner = new Scanner(input);		
		//System.out.println("Start: " + System.nanoTime());
		while (scanner.hasNext()) {					
			firstChar = scanner.getNext();		
			if (scanner.stream.indexOf(firstChar, scanner.getCurrentPosition()) < 0) {				
				//System.out.println("End: " + System.nanoTime());
				return firstChar;
			}
		}
				
		return returnChar;
		
	}
}
