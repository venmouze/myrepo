package interfaces;
/*
 * Interface Stream - para implementar m�todos de leitura da stream
 * 
 * @author Willian
 */
public interface Stream {
	
	public boolean hasNext();
	public char getNext();
	
}
