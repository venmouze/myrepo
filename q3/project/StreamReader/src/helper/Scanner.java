package helper;
import interfaces.Stream;

/*
 * Classe Scanner - respons�vel por validar, armazenar e controlar posicionamento de leitura da stream.
 * 
 * @author Willian
 * */
public class Scanner implements Stream {

	private boolean hasNext;
	private char nextChar;
	public String stream = null;
	private int currentPosition;
	private int lastPosition;
	
	/*
	 * Construtor
	 * */
	public Scanner(String stream) {
		this.hasNext = hasNext;
		this.nextChar = nextChar;
		this.stream = stream;
		lastPosition = stream.length();
	}
	
	
	/*
	 * M�todo verifica o posicionamento da stream e valida se 
	 * � poss�vel continuar o procssamento ou n�o.
	 * 
	 * @return boolean - Retorna true/false para saber se ainda h� 
	 * caracteres para processar.
	 * */
	@Override
	public boolean hasNext() {
		if (currentPosition < lastPosition) {
			return true;
		} else {
			System.out.println("No char available to check.");
		}
		
		return hasNext;
	}

	/*
	 * M�todo busca o pr�ximo caracter e incrementa a posi��o atual do indice.
	 * 
	 * @return char - Retorna char referente ao processamento atual.	 
	 * */
	@Override
	public char getNext() {
		
		nextChar = stream.charAt(currentPosition);			
		currentPosition++;
		
		return nextChar;
	}	
	
	/*
	 * M�todo auxiliar para informar a posi��o atual da stream.
	 * 
	 * @return int - Retorna int com n�mero da posi��o atual da stream.
	 * */
	public int getCurrentPosition() {		
		return currentPosition;
	}
	
	/*
	 * M�todo auxiliar para informar �ltima posi��o da stream.
	 * 
	 * @return int - Retorna int com n�mero da �ltima posi��o da stream.
	 * */
	public int getLastPosition() {		
		return lastPosition;
	}	
}
