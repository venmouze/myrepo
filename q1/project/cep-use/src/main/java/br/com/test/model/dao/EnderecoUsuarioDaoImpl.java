package br.com.test.model.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.test.model.EnderecoUsuario;
import br.com.test.util.HibernateUtils;

public class EnderecoUsuarioDaoImpl implements EnderecoUsuarioDao {

	public void salvar(EnderecoUsuario enderecoUsuario) {
		Session session = HibernateUtils.getSessionFactory().openSession();
		Transaction t = session.beginTransaction();
		session.save(enderecoUsuario);
		t.commit();
	}

	public EnderecoUsuario getEnderecoUsuarioById(long id) {
		Session session = HibernateUtils.getSessionFactory().openSession();
		return (EnderecoUsuario) session.load(EnderecoUsuario.class, id);
	}

	public void remover(EnderecoUsuario enderecoUsuario) {
		Session session = HibernateUtils.getSessionFactory().openSession();
		Transaction t = session.beginTransaction();
		session.delete(enderecoUsuario);
		t.commit();
	}

	public void atualizar(EnderecoUsuario enderecoUsuario) {
		Session session = HibernateUtils.getSessionFactory().openSession();
		Transaction t = session.beginTransaction();
		session.update(enderecoUsuario);
		t.commit();
	}
}
