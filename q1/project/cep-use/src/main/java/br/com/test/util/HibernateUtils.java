package br.com.test.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import br.com.test.model.EnderecoUsuario;

public class HibernateUtils {
	
	private static SessionFactory sessionFactory;
	
	public HibernateUtils() {
	}

	public static SessionFactory getSessionFactory() {
		if (sessionFactory == null) {
			try {
				Configuration ac = new Configuration();
				ac.addAnnotatedClass(EnderecoUsuario.class);
				sessionFactory = ac.configure().buildSessionFactory();
			} catch (Throwable ex) {
				// Log the exception.
				System.err.println("Initial SessionFactory creation failed." + ex);
				throw new ExceptionInInitializerError(ex);
			}
			return sessionFactory;
		} else {
			return sessionFactory;
		}
	}
}
