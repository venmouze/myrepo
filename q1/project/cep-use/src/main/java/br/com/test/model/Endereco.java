package br.com.test.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Endereco {
	@Id
	@Column(name = "cep_id", nullable = true)
	private Integer cepId;
	
	private String endereco;
	private String cidade;
	private String estado;
	private String bairro;

	public Integer getCepId() {
		return cepId;
	}

	public void setCepId(Integer cepId) {
		this.cepId = cepId;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
}
