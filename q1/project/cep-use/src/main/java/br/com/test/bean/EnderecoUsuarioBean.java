package br.com.test.bean;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.com.test.model.EnderecoUsuario;
import br.com.test.util.EntityManagerHelper;

public class EnderecoUsuarioBean {
  
	  private EntityManager entityManager;
	 
	  private EnderecoUsuario enderecoUsuario = new EnderecoUsuario();
	 
	  private Long id;
	 
	  private List<EnderecoUsuario> list;
	 
	  public String persist() {

		  try {
		      EntityTransaction entityTransaction = getEntityManager().getTransaction();
		 
		      entityTransaction.begin();
		      getEntityManager().persist(enderecoUsuario);
		      entityTransaction.commit();
		 
		      newInstance();
		      list = null;
		 
		      return "ok";
	    } catch (Exception e) {
	      e.printStackTrace();
	      return "nok";
	    }
	  }
	 
	  public String update() {
	    try {
	      EntityTransaction entityTransaction = getEntityManager().getTransaction();
	 
	      entityTransaction.begin();
	      getEntityManager().merge(enderecoUsuario);
	      entityTransaction.commit();
	 
	      newInstance();
	 
	      return "ok";
	    } catch (Exception e) {
	      e.printStackTrace();
	      return "nok";
	    }
	  }
	 
	  public String remove() {
	    try {
	      EntityTransaction entityTransaction = getEntityManager().getTransaction();
	 
	      entityTransaction.begin();
	      getEntityManager().remove(entityTransaction);
	      entityTransaction.commit();
	 
	      newInstance();
	      list = null;
	 
	      return "sucesso";
	    } catch (Exception e) {
	      e.printStackTrace();
	      return "falhou";
	    }
	  }
	  
	  public EntityManager getEntityManager() {
	    if (entityManager == null) {
	      entityManager = EntityManagerHelper.getInstance().createEntityManager();
	    }
	 
	    return entityManager;
	  }
	 
	  public void setEnderecoUsuario(EnderecoUsuario enderecoUsuario) {
	    this.enderecoUsuario = enderecoUsuario;
	  }
	 
	  public EnderecoUsuario getEnderecoUsuario() {
	    return enderecoUsuario;
	  }
	 
	  public void setId(Long id) {
	    this.id = id;
	    if (id != null) {
	      enderecoUsuario = getEntityManager().find(EnderecoUsuario.class, id);
	    }
	  }
	 
	  public Long getId() {
	    return id;
	  }
	 
	  public void newInstance() {
		  enderecoUsuario = new EnderecoUsuario();
	  }
}
