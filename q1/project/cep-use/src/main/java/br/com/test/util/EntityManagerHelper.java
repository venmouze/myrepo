package br.com.test.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerHelper {
	
	private static final EntityManagerHelper instance = new EntityManagerHelper();

	private static final EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("cep-use");

	private EntityManagerHelper() {
	}

	public static EntityManagerHelper getInstance() {
		return instance;
	}

	public EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}

	public EntityManager createEntityManager() {
		return entityManagerFactory.createEntityManager();
	}
}
