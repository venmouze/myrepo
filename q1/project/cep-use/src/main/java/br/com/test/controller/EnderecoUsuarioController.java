package br.com.test.controller;

import javax.annotation.ManagedBean;
import javax.faces.bean.SessionScoped;

import br.com.test.model.EnderecoUsuario;
import br.com.test.model.dao.EnderecoUsuarioDao;
import br.com.test.model.dao.EnderecoUsuarioDaoImpl;

@ManagedBean
@SessionScoped
public class EnderecoUsuarioController {

	private EnderecoUsuario enderecoUsuario;

	public EnderecoUsuario getEnderecoUsuario() {
		return enderecoUsuario;
	}

	public void setEnderecoUsuario(EnderecoUsuario enderecoUsuario) {
		this.enderecoUsuario = enderecoUsuario;
	}

	public String prepararEnderecoUsuario() {
		enderecoUsuario = new EnderecoUsuario();
		return "gerenciarEnderecoUsuario";
	}

	public String excluirEnderecoUsuario() {
		EnderecoUsuarioDao dao = new EnderecoUsuarioDaoImpl();
		dao.remover(enderecoUsuario);
		return "index";
	}

	public String adicionarEnderecoUsuario() {
		EnderecoUsuarioDao dao = new EnderecoUsuarioDaoImpl();
		dao.salvar(enderecoUsuario);
		return "index";
	}

	public String alterarEnderecoUsuario() {
		EnderecoUsuarioDao dao = new EnderecoUsuarioDaoImpl();
		dao.atualizar(enderecoUsuario);
		return "index";
	}

}
