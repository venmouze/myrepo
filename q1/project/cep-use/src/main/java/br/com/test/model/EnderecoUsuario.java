package br.com.test.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class EnderecoUsuario {
	
	@Id
	@GeneratedValue
	@Column(name = "endereco_id", nullable = true)
	private Integer enderecoId;
	
	@Column(name = "nome_referencia")
	private String nomeReferencia;
	private Integer numero;
	private String complemento;
	private Long usuarioId;
	private Long cepId;
		
	public String getNomeReferencia() {
		return nomeReferencia;
	}

	public void setNomeReferencia(String nomeReferencia) {
		this.nomeReferencia = nomeReferencia;
	}

	public Integer getEnderecoId() {
		return enderecoId;
	}
	
	public void setEnderecoId(Integer enderecoId) {
		this.enderecoId = enderecoId;
	}
	
	public Integer getNumero() {
		return numero;
	}
	
	public void setNumero(Integer numero) {
		this.numero = numero;
	}
	
	public String getComplemento() {
		return complemento;
	}
	
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	
	public Long getUsuarioId() {
		return usuarioId;
	}
	
	public void setUsuarioId(Long usuarioId) {
		this.usuarioId = usuarioId;
	}
	
	public Long getCepId() {
		return cepId;
	}
	
	public void setCepId(Long cepId) {
		this.cepId = cepId;
	}
}
