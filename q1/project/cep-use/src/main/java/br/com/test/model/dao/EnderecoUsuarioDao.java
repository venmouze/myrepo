package br.com.test.model.dao;

import br.com.test.model.EnderecoUsuario;

public interface EnderecoUsuarioDao {

	public EnderecoUsuario getEnderecoUsuarioById(long id);
	public void salvar(EnderecoUsuario enderecoUsuario);
	public void remover(EnderecoUsuario enderecoUsuario);
	public void atualizar(EnderecoUsuario enderecoUsuario);	
}
