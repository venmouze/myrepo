<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>Cadastro de Endereço</title>
        </head>
        <body>
            <h:form>
                <div align="center">
                    <h:panelGrid columns="5">
                        <h:outputLabel value="Nome Referência" for="referenciaEndereco" />
                        <h:inputText id="referenciaEndereco" value="#{enderecoUsuarioBean.enderecoUsuario.nomeReferencia}" />
  
                        <h:outputLabel value="CEP" for="cep" />
                        <h:inputText id="cep" value="#{getAddressBean}" />
  
                        <h:outputLabel value="Número" for="numero" />
                        <h:inputText id="numero" value="#{enderecoUsuarioBean.enderecoUsuario.numero}" />
                        
                        <h:outputLabel value="Complemento" for="complemento" />
                        <h:inputText id="complemento" value="#{enderecoUsuarioBean.enderecoUsuario.complemento}" />                        
  
                        <h:outputLabel value="Cidade" for="cidade" />
                        <h:inputText id="cidade" value="#{}" />

                        <h:outputLabel value="Estado" for="estado" />
                        <h:inputText id="estado" value="#{}" />
                        
                        <h:outputLabel value="Bairro" for="bairro" />
                        <h:inputText id="bairro" value="#{}" />            
  
                    </h:panelGrid>
  
                    <h:commandButton value="Cadastrar" action="#{enderecoUsuarioBean.persist}" />
                </div>
  
                <br />
                <hr width="50%"/>
                <br />
            </h:form>
        </body>
    </html>
</f:view>